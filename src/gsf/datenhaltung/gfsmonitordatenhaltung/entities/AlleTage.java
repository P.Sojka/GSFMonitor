/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gsf.datenhaltung.gfsmonitordatenhaltung.entities;

import java.util.ArrayList;

/**
 *
 * @author vince
 */
public class AlleTage {
    
    private Wochentag wochentag;
    private double grundlast;
    private double mittellast;
    private double spitzenlast;
    private Kategorie typ;
    
    private ArrayList<Tag> tage = new ArrayList<>();
    
    public void addTag(Tag tag){
        tage.add(tag);
    }
    public ArrayList<Tag> getTage() {
        return tage;
    }

    public void setTage(ArrayList<Tag> tage) {
        this.tage = tage;
    }
    
    public Wochentag getWochentag() {
        return wochentag;
    }

    public void setWochentag(Wochentag wochentag) {
        this.wochentag = wochentag;
    }

    public double getGrundlast() {
        return grundlast;
    }

    public void setGrundlast(double grundlast) {
        this.grundlast = grundlast;
    }

    public double getMittellast() {
        return mittellast;
    }

    public void setMittellast(double mittellast) {
        this.mittellast = mittellast;
    }

    public double getSpitzenlast() {
        return spitzenlast;
    }

    public void setSpitzenlast(double spitzenlast) {
        this.spitzenlast = spitzenlast;
    }

    public Kategorie getTyp() {
        return typ;
    }

    public void setTyp(Kategorie typ) {
        this.typ = typ;
    }
    
}
