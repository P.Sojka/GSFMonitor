/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gsf.datenhaltung.gfsmonitordatenhaltung.entities;

import java.util.ArrayList;

/**
 *
 * @author vince
 */
public class Lastprofil {
    
    private Kategorie typ;
    
    private ArrayList<Datensatz> datensaetze = new ArrayList<>();
    private ArrayList<Tag> besteht = new ArrayList<>();
    
    //0 Sonntag, 1 Montag, 2 Dienstag, 3 Mittwoch, 4 Donnerstag, 5 Freitag, 6 Samstag
    private ArrayList<AlleTage> alleTage = new ArrayList<>(7);

    public Kategorie getTyp() {
        return typ;
    }

    public ArrayList<Datensatz> getDatensaetze() {
        return datensaetze;
    }

    public void setDatensaetze(ArrayList<Datensatz> datensaetze) {
        this.datensaetze = datensaetze;
    }

    public ArrayList<Tag> getBesteht() {
        return besteht;
    }

    public void setBesteht(ArrayList<Tag> besteht) {
        this.besteht = besteht;
    }
    
    public void addTag(Tag tag){
        besteht.add(tag);
    }

    public ArrayList<AlleTage> getWochentage() {
        return alleTage;
    }
    
    public AlleTage getWochentagByIndex(int index){
        return alleTage.get(index);
    }

    public void setWochentage(ArrayList<AlleTage> hat) {
        this.alleTage = hat;
    }
    
    public void addWochentag(AlleTage tag){
        alleTage.add(tag);
    }

    public void setTyp(Kategorie typ) {
        this.typ = typ;
    }

    public void addDatensatz(Datensatz datensatz){
        this.datensaetze.add(datensatz);
    }
    
}
