/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gsf.datenhaltung.gfsmonitordatenhaltung.entities;

import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author vince
 */
public class Tag {
    
    private Date datum; 
    private Wochentag wochentag; 
    private double grundlast; 
    private double mittellast;
    private double spitzenlast;
    private Kategorie typ;
    
    private ArrayList<Datensatz> datensaetze = new ArrayList<>(96);
    private Lastprofil gehoertZu;

    public Wochentag getWochentag() {
        return wochentag;
    }

    public void setWochentag(Wochentag wochentag) {
        this.wochentag = wochentag;
    }

    public ArrayList<Datensatz> getDatensaetze() {
        return datensaetze;
    }

    public void setDatensaetze(ArrayList<Datensatz> datensaetze) {
        this.datensaetze = datensaetze;
    }

    public Lastprofil getGehoertZu() {
        return gehoertZu;
    }

    public void setGehoertZu(Lastprofil gehoertZu) {
        this.gehoertZu = gehoertZu;
    }
    
    
    public Date getDatum() {
        return datum;
    }

    public void setDatum(Date datum) {
        this.datum = datum;
    }

    public double getGrundlast() {
        return grundlast;
    }

    public void setGrundlast(double grundlast) {
        this.grundlast = grundlast;
    }

    public double getMittellast() {
        return mittellast;
    }

    public void setMittellast(double mittellast) {
        this.mittellast = mittellast;
    }

    public double getSpitzenlast() {
        return spitzenlast;
    }

    public void setSpitzenlast(double spitzenlast) {
        this.spitzenlast = spitzenlast;
    }
    
    public void addDatensatz(Datensatz datensatz){
        this.datensaetze.add(datensatz);
    }

    public Kategorie getTyp() {
        return typ;
    }

    public void setTyp(Kategorie typ) {
        this.typ = typ;
    }
    
    
    
}
