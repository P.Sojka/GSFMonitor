/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gsf.fachlogik.gsfmonitorsteuerung.service;

import gsf.datenhaltung.gfsmonitordatenhaltung.entities.AlleTage;
import gsf.datenhaltung.gfsmonitordatenhaltung.entities.Datensatz;
import gsf.datenhaltung.gfsmonitordatenhaltung.entities.Kategorie;
import gsf.datenhaltung.gfsmonitordatenhaltung.entities.Lastprofil;
import gsf.datenhaltung.gfsmonitordatenhaltung.entities.Tag;
import gsf.datenhaltung.gfsmonitordatenhaltung.entities.Wochentag;
import gsf.gui.gsfmonitorgui.gui.LastDiagramm1;
import gsf.jfreechart.impl.GSFMonitorChartImpl;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import org.jfree.data.category.DefaultCategoryDataset;
import org.joda.time.DateTime;
import org.joda.time.Duration;

/**
 *
 * @author Patrick Sojka, Nikita Gromow, Yasin Mantas
 */
public class IGSFMonitorSteuer {

    private GSFMonitorChartImpl jfreechart = new GSFMonitorChartImpl();
    private String diagramm;

    private static Lastprofil gas = null;
    private static Lastprofil strom = null;
    private static Lastprofil fernwaerme = null;

    public IGSFMonitorSteuer() {

        //Lastprofil lastprofil = lastProfilDemo();
        //lastprofil = this.datenInterpolieren(lastprofil);
        //this.grundlastBerechnen(gas);
        //this.mittellastBerechnen(lastprofil);
        //this.spitzenlastBerechnen(lastprofil);
    }

    public void testLastProfil() {
        this.grundlastBerechnen(gas);
        this.mittellastBerechnen(gas);
        this.spitzenlastBerechnen(gas);
    }

    /**
     * Löscht den Datensatz wirklich
     */
    public void gasReset() {
        gas = null;
    }

    /**
     * Löscht den Datensatz wirklich
     */
    public void stromReset() {
        strom = null;
    }

    /**
     * Löscht den Datensatz wirklich
     */
    public void fernwaermeReset() {
        fernwaerme = null;
    }

    /**
     * Diese Methode erstellt für die JFreeChart Komponente ein Datensatz zur
     * erzeugung eines Liniendiagramms.
     *
     * @param lastp Kategorie 0 = gas,1 = Strom, 2 =Fernwärme;
     * @return Pfad zur .png für die gui;
     */
    public String lastprofilAnzeigenTimeSeries(int lastp) {

        final DefaultCategoryDataset dataset = new DefaultCategoryDataset();
        ArrayList<Datensatz> daten = null;

        String typ = "";
        try {
            switch (lastp) {
                case 0:
                    daten = gas.getDatensaetze();
                    typ = "Gas";
                    break;
                case 1:
                    daten = strom.getDatensaetze();
                    typ = "Strom";
                    break;
                case 2:
                    daten = fernwaerme.getDatensaetze();
                    typ = "Fernwaerme";
                    break;
                default:
                    break;
            }
        } catch (Exception e) {
            System.out.println("Kein Lastprofil" + e.getMessage());
        }
        String jahr;
        Double wert;

        for (int i = 0; i < daten.size(); i++) {
            try {

                wert = daten.get(i).getVerbrauch();

                jahr = daten.get(i).getDatum().toString();

                dataset.addValue(wert, "Verbrauch", jahr);
            } catch (Exception e) {
                System.out.println("Fehler" + e.getMessage());
            }
        }

        /**
         * **** LastDiagramm1.java *********
         */
        LastDiagramm1 lastDiagramm1 = new LastDiagramm1();
        DecimalFormat d = new DecimalFormat("#.##");
        String endung = "";
        double lastGrundLabel = 0;
        double lastMittelLabel = 0;
        double lastSpitzenLabel = 0;
        String dia1KategorieLabel = typ;
        if (lastp == 0) {
            lastGrundLabel = this.grundlastBerechnen(gas);
            lastMittelLabel = this.mittellastBerechnen(gas);
            lastSpitzenLabel = this.spitzenlastBerechnen(gas);
            endung = " (Mrd./m^3)";
        } else if (lastp == 1) {
            lastGrundLabel = this.grundlastBerechnen(strom);
            lastMittelLabel = this.mittellastBerechnen(strom);
            lastSpitzenLabel = this.spitzenlastBerechnen(strom);
            endung = " (kWh)";
        } else if (lastp == 2) {
            lastGrundLabel = this.grundlastBerechnen(fernwaerme);
            lastMittelLabel = this.mittellastBerechnen(fernwaerme);
            lastSpitzenLabel = this.spitzenlastBerechnen(fernwaerme);
            endung = " (MWh)";
        } else {

        }

        lastDiagramm1.dia1KategorieLabel.setText(typ);

        lastDiagramm1.lastGrundLabel.setText(d.format(lastGrundLabel) + endung);
        lastDiagramm1.lastMittelLabel.setText(d.format(lastMittelLabel) + endung);
        lastDiagramm1.lastSpitzenLabel.setText(d.format(lastSpitzenLabel) + endung);

        lastDiagramm1.setVisible(true);
        /**
         * **** LastDiagramm1.java *********
         */

        return this.jfreechart.TimeSeries(dataset, typ);
    }

    /**
     * Diese Methode erstellt für die JFreeChart Komponente ein Datensatz zur
     * erzeugung eines Liniendiagramms. Dieser Datensatz ist durch zwei Daten
     * eingegrenzt.
     *
     * @param lastp Kategorie 0 = gas,1 = Strom, 2 =Fernwärme;
     * @param von erstes datum zum eingrenzen
     * @param bis letzes datum zum eingrenzen
     * @return Pfad zur .png für die gui;
     */
    public String lastprofilAnzeigenTimeSeries(int lastp, Date von, Date bis) {

        final DefaultCategoryDataset dataset = new DefaultCategoryDataset();
        ArrayList<Datensatz> daten = null;
        String typ = "";
        try {
            switch (lastp) {
                case 0:
                    daten = gas.getDatensaetze();
                    typ = "Gas";
                    break;
                case 1:
                    daten = strom.getDatensaetze();
                    typ = "Strom";
                    break;
                case 2:
                    daten = fernwaerme.getDatensaetze();
                    typ = "Fernwaerme";
                    break;
                default:
                    break;
            }
        } catch (Exception e) {
            System.out.println("Kein Lastprofil" + e.getMessage());
        }

        if (von.getYear() == 0 && bis.getYear() == 0) {
            int jahrHilf = daten.get(0).getDatum().getYear();

            von.setYear(jahrHilf);
            bis.setYear(jahrHilf);

            //wenn Winter
            if (von.getDate() == 21 && von.getMonth() == 11) {
                bis.setYear(jahrHilf + 1);

                //von.setYear(jahrHilf-1);
                //bis.setYear(jahrHilf);
            }

        }

        String datum;
        Double wert;
        Date dateAktuellerDatensatz;

        for (int i = 0; i < daten.size(); i++) {
            try {
                dateAktuellerDatensatz = (Date) daten.get(i).getDatum().clone();
                dateAktuellerDatensatz.setHours(0);
                dateAktuellerDatensatz.setMinutes(0);
                if (dateAktuellerDatensatz.compareTo(von) == 0) {
                    wert = daten.get(i).getVerbrauch();
                    datum = daten.get(i).getDatum().toString();

                    dataset.addValue(wert, "Verbrauch", datum);
                }
                if (dateAktuellerDatensatz.after(von) && dateAktuellerDatensatz.before(bis)) {
                    wert = daten.get(i).getVerbrauch();
                    datum = daten.get(i).getDatum().toString();

                    dataset.addValue(wert, "Verbrauch", datum);
                }
                if (dateAktuellerDatensatz.compareTo(bis) == 0 && von.compareTo(bis) != 0) {
                    wert = daten.get(i).getVerbrauch();
                    datum = daten.get(i).getDatum().toString();

                    dataset.addValue(wert, "Verbrauch", datum);
                }
            } catch (Exception e) {
                System.out.println("Fehler" + e.getMessage());
            }
        }

        return this.jfreechart.TimeSeries(dataset, typ);
    }

    /**
     * Diese Methode erstellt für die JFreeChart Komponente ein Datensatz zur
     * erzeugung eines Säulendiagramms.
     *
     * @param lastp Kategorie 0 = gas,1 = Strom, 2 =Fernwärme;
     * @param tag 0=Sonntag, 1=Montag, 2=Dienstag, 3=Mittwoch,
     * 4=Donnerstag,5=Freitag, 6=Samstag;
     * @return Pfad zur .png für die gui;
     */
    public String einTagAnzeigen(int lastp, int tag) {

        final DefaultCategoryDataset dataset = new DefaultCategoryDataset();

        final String grundlast = "Grundlast";
        final String mittellast = "Mittellast";
        final String spitzenlast = "Spitzenlast";

        String wochentag;
        String typ = "";

        AlleTage einTag = null;
        try {
            switch (lastp) {
                case 0:

                    einTag = gas.getWochentagByIndex(tag);
                    typ = "Gas";
                    break;
                case 1:

                    einTag = strom.getWochentagByIndex(tag);
                    typ = "Strom";
                    break;
                case 2:
                    einTag = fernwaerme.getWochentagByIndex(tag);
                    typ = "Fernwaerme";
                    break;
                default:
                    break;
            }
        } catch (Exception e) {
            System.out.println("Kein Lastprofil" + e.getMessage());
        }
        wochentag = this.erkenneWochentag(tag).toString();

        String showDate;
        int month;
        try {
            for (Tag tag1 : einTag.getTage()) {
                try {
                    month = tag1.getDatum().getMonth() + 1;
                    showDate = tag1.getDatum().getDate() + "." + month;

                    dataset.addValue(tag1.getGrundlast(), grundlast, showDate);
                    dataset.addValue(tag1.getMittellast(), mittellast, showDate);
                    dataset.addValue(tag1.getSpitzenlast(), spitzenlast, showDate);
                } catch (Exception e) {
                    System.out.println("Fehler" + e.getMessage());
                }
            }
        } catch (Exception e) {
            System.out.println("Fehler" + e.getMessage());
        }
        return this.jfreechart.BarChart(dataset, typ);
    }

    /**
     * Diese Methode erstellt für die JFreeChart Komponente ein Datensatz zur
     * erzeugung eines Säulendiagramms.
     *
     * @param lastp Kategorie 0 = gas,1 = Strom, 2 =Fernwärme;
     * @return Pfad zur .png für die gui;
     */
    public String wochenAnsichtAnzeigen(int lastp) {

        final DefaultCategoryDataset dataset = new DefaultCategoryDataset();

        final String grundlast = "Grundlast";
        final String mittellast = "Mittellast";
        final String spitzenlast = "Spitzenlast";

        String typ = "";
        ArrayList<AlleTage> daten = null;

        try {
            switch (lastp) {
                case 0:
                    daten = gas.getWochentage();
                    typ = "Gas";
                    break;
                case 1:
                    daten = strom.getWochentage();
                    typ = "Strom";
                    break;
                case 2:
                    daten = fernwaerme.getWochentage();
                    typ = "Fernwaerme";
                    break;
                default:
                    break;
            }
        } catch (Exception e) {
            System.out.println("Kein Lastprofil" + e.getMessage());
        }
        for (int i = 0; i < 7; i++) {
            try {
                dataset.addValue(daten.get(i).getGrundlast(), grundlast, this.erkenneWochentag(i).toString());
                dataset.addValue(daten.get(i).getMittellast(), mittellast, this.erkenneWochentag(i).toString());
                dataset.addValue(daten.get(i).getSpitzenlast(), spitzenlast, this.erkenneWochentag(i).toString());
            } catch (Exception e) {
                System.out.println("Fehler" + e.getMessage());
            }
        }

        return this.jfreechart.BarChart(dataset, typ);

    }

    /**
     * Vom gegebenen Lastprofil wird ein Grundlast berechnet.
     * Ref:https://blog.stromhaltig.de/2013/06/lastprofillastganganalyse-synthetische-verteilung-der-verbrauchsmengen/
     *
     * @param lastprofil
     * @return
     */

    /* Ref : https://blog.stromhaltig.de/2013/06/lastprofillastganganalyse-synthetische-verteilung-der-verbrauchsmengen/ */
    public double grundlastBerechnen(Lastprofil lastprofil) {
        // Grundlast = AvgP - ArAvgP = 44,779
        ArrayList<Double> deltaList = getDelta(lastprofil);
        Double AvgP = this.getMittelwert(deltaList);
        Double ArAvgP = this.arAvgP(deltaList);
        Double Grundlast = AvgP - ArAvgP;
        return Grundlast;
    }

    /**
     * Die Rohdaten enthalten Zählerstände, daher muss zunächst über das Delta
     * der Zählerstände der Verbrauch zwischen den Ablesungen bestimmt werden:
     * C3 = B3-B2, C4=B4-B3, - , C1979=B1979-B1978
     *
     * @param lastprofil
     * @return
     */
    private ArrayList<Double> getDelta(Lastprofil lastprofil) {

        ArrayList<Datensatz> datensatzList = lastprofil.getDatensaetze();

        /* ERSTELLEN VON ARRAYLIST DOUBLE */
        ArrayList<Double> deltaList = new ArrayList<Double>();

        Iterator<Datensatz> it = datensatzList.iterator();
        Double erg = 0.0;
        Double before = 0.0;
        while (it.hasNext()) {
            Datensatz current = it.next();

            if (before != 0.0) {
                erg = (current.getVerbrauch() - before);
                deltaList.add(erg);
            }
            before = current.getVerbrauch();
        }
        return deltaList;

    }

    /**
     * MITTELABW() - Mittlere Abweichung
     *
     * @param delta
     * @return
     */
    private Double arAvgP(ArrayList<Double> delta) {

        int size = delta.size();

        Double mittelwert = this.getMittelwert(delta);
        Double bruch = 1.0 / size;

        Double akt = 0.0;
        Double summe = 0.0;
        Double ret = 0.0;
        for (int i = 0; i < size; i++) {
            akt = delta.get(i);
            summe = summe + Math.abs(akt - mittelwert);

        }
        ret = bruch * summe;
        return ret;
    }

    /**
     * Liefert Mittelwert zurück.
     *
     * @param dlist
     * @return
     */
    private Double getMittelwert(ArrayList<Double> dlist) {

        Double summe = 0d;
        for (Double vals : dlist) {
            summe += vals;
        }

        summe = summe / dlist.size();
        return summe;
    }

    /**
     * http://www.mathebibel.de/mittlere-absolute-abweichung
     *
     * @param lastprofil
     * @return
     */
    private double mittellastBerechnen(Lastprofil lastprofil) {
        Double mittellast = 0.0;
        ArrayList<Double> deltaList = this.getDelta(lastprofil);
        mittellast = this.getMittelwert(deltaList);
        return mittellast;
    }

    /**
     * http://www.mathebibel.de/mittlere-absolute-abweichung
     *
     * @param lastprofil
     * @return
     */
    private double spitzenlastBerechnen(Lastprofil lastprofil) {
        // Spitzenlast = AvgP + ArAvgP = 128,720
        ArrayList<Double> deltaList = this.getDelta(lastprofil);
        Double AvgP = this.getMittelwert(deltaList);
        Double ArAvgP = this.arAvgP(deltaList);
        Double Spitzenlast = AvgP + ArAvgP;
        return Spitzenlast;
    }

    /**
     * Diese Methode exportiert das ausgewählte Diagramm (TimeSeries ||
     * BarChart) in einer höheren Auflösung.
     *
     * @param diagKat diagKat.get(0) steht für das Diagramm1 || Diagramm2,
     * diagKat.get(1) steht für die Kategorie Gas || Strom || FW, diagKat.get(2)
     * steht für den Wochentag Sonntag = 0 || Montag = 1 || ... || AlleTage = 7
     * @param von
     * @param bis
     * @param ttyp steht für csv || png
     * @param jType steht für T = TimeSeries || B = BarChart
     * @return
     */
    public boolean datenExportieren(ArrayList<Integer> diagKat, Date von, Date bis, String ttyp, String jTyp) {
        int aktJahr;
        String bildHD = "";
        int aktDiagramm = diagKat.get(0);
        int aktKat = diagKat.get(1);
        int wochenTag = 99;
        Tag tag;
        try {
            wochenTag = diagKat.get(2);
        } catch (Exception e) {

        }

        if (von.getYear() == 1900 || bis.getYear() == 1900) {
            if (aktKat == 0) {
                aktJahr = gas.getDatensaetze().get(0).getDatum().getYear();
            }
            if (aktKat == 1) {
                aktJahr = strom.getDatensaetze().get(0).getDatum().getYear();
            }
            if (aktKat == 2) {
                aktJahr = fernwaerme.getDatensaetze().get(0).getDatum().getYear();
            }
            von.setYear(aktKat);
            bis.setYear(aktKat);
        }

        //TimeSeries
        if (jTyp.equals("T") && ttyp.equals("png")) {
            final DefaultCategoryDataset dataset = new DefaultCategoryDataset();
            ArrayList<Datensatz> daten = null;
            String typ = "";

            switch (aktKat) {
                case 0:
                    daten = gas.getDatensaetze();
                    typ = "Gas";
                    break;
                case 1:
                    daten = strom.getDatensaetze();
                    typ = "Strom";
                    break;
                case 2:
                    daten = fernwaerme.getDatensaetze();
                    typ = "Fernwaerme";
                    break;
                default:
                    break;
            }
            String datum;
            Double wert;
            Date dateAktuellerDatensatz;

            for (int i = 0; i < daten.size(); i++) {
                dateAktuellerDatensatz = (Date) daten.get(i).getDatum().clone();
                dateAktuellerDatensatz.setHours(0);
                dateAktuellerDatensatz.setMinutes(0);
                if (dateAktuellerDatensatz.compareTo(von) == 0) {
                    wert = daten.get(i).getVerbrauch();
                    datum = daten.get(i).getDatum().toString();

                    dataset.addValue(wert, "Verbrauch", datum);
                }
                if (dateAktuellerDatensatz.after(von) && dateAktuellerDatensatz.before(bis)) {
                    wert = daten.get(i).getVerbrauch();
                    datum = daten.get(i).getDatum().toString();

                    dataset.addValue(wert, "Verbrauch", datum);
                }
                if (dateAktuellerDatensatz.compareTo(bis) == 0 && von.compareTo(bis) != 0) {
                    wert = daten.get(i).getVerbrauch();
                    datum = daten.get(i).getDatum().toString();

                    dataset.addValue(wert, "Verbrauch", datum);
                }

            }
            bildHD = jfreechart.TimeSeriesHD(dataset, typ);

        }

        /**
         * ****** BarChart AlleTage HD ********
         */
        if (jTyp.equals("B") && ttyp.equals("png") && (diagKat.size() == 3 && wochenTag == 7)) {
            final DefaultCategoryDataset dataset = new DefaultCategoryDataset();

            final String grundlast = "Grundlast";
            final String mittellast = "Mittellast";
            final String spitzenlast = "Spitzenlast";

            String typ = "";
            ArrayList<AlleTage> daten = null;

            try {
                switch (aktKat) {
                    case 0:
                        daten = gas.getWochentage();
                        typ = "Gas";
                        break;
                    case 1:
                        daten = strom.getWochentage();
                        typ = "Strom";
                        break;
                    case 2:
                        daten = fernwaerme.getWochentage();
                        typ = "Fernwaerme";
                        break;
                    default:
                        break;
                }
            } catch (Exception e) {
                System.out.println("Kein Lastprofil" + e.getMessage());
            }
            for (int i = 0; i < 7; i++) {

                dataset.addValue(daten.get(i).getGrundlast(), grundlast, this.erkenneWochentag(i).toString());
                dataset.addValue(daten.get(i).getMittellast(), mittellast, this.erkenneWochentag(i).toString());
                dataset.addValue(daten.get(i).getSpitzenlast(), spitzenlast, this.erkenneWochentag(i).toString());

            }
            bildHD = jfreechart.BarChartHD(dataset, typ);

        }

        /**
         * *********************** BarChart Wochentag HD **********************
         */
        if (jTyp.equals("B") && ttyp.equals("png") && (diagKat.size() == 3 && wochenTag != 7)) {
            final DefaultCategoryDataset dataset = new DefaultCategoryDataset();
            //verschiedene Bereiche (farblich)
            final String grundlast = "Grundlast";
            final String mittellast = "Mittellast";
            final String spitzenlast = "Spitzenlast";
            //Kategorien
            String typ = "";
            tag = new Tag();
            tag.setWochentag(this.erkenneWochentag(wochenTag));
            AlleTage einTag = null;
            try {
                switch (aktKat) {
                    case 0:
                        einTag = gas.getWochentagByIndex(wochenTag);
                        typ = "Gas";
                        break;
                    case 1:
                        einTag = strom.getWochentagByIndex(wochenTag);
                        typ = "Strom";
                        break;
                    case 2:
                        einTag = fernwaerme.getWochentagByIndex(wochenTag);
                        typ = "Fernwaerme";
                        break;
                    default:
                        break;
                }
            } catch (Exception e) {
                System.out.println("Kein Lastprofil" + e.getMessage());
            }

            /*
            int i = 0;
            for (Tag tag1 : einTag.getTage()) {

                dataset.addValue(tag1.getGrundlast(), grundlast, "" + i);
                dataset.addValue(tag1.getMittellast(), mittellast, "" + i);
                dataset.addValue(tag1.getSpitzenlast(), spitzenlast, "" + i);
                i++;
            }
             */
            String showDate;
            int month;
            for (Tag tag1 : einTag.getTage()) {

                month = tag1.getDatum().getMonth() + 1;
                showDate = tag1.getDatum().getDate() + "." + month;
                //Werte
                dataset.addValue(tag1.getGrundlast(), grundlast, showDate);
                dataset.addValue(tag1.getMittellast(), mittellast, showDate);
                dataset.addValue(tag1.getSpitzenlast(), spitzenlast, showDate);
                //i++;

            }

            bildHD = jfreechart.BarChartHD(dataset, typ);

        }

        return true;
    }

    /**
     * Fehlende Datensätze eines Lastprofils werden nachgebilden und die
     * Verbrauchstwerte werden interpoliert.
     *
     * @param lastprofil
     * @return Lastprofil
     */
    private Lastprofil datenInterpolieren(Lastprofil lastprofil) {
        ArrayList<Datensatz> arr = lastprofil.getDatensaetze();
        ArrayList<Datensatz> arrNew = new ArrayList<>();
        Datensatz vor;
        Datensatz akt;
        Datensatz danach;

        double newVerbrauch;

        Date aktDate, nextDate, newDate;
        Tag newTag = new Tag();
        Lastprofil newLastprofil = new Lastprofil();

        for (int i = 0; i < arr.size(); i++) {

            akt = arr.get(i);//aktuelles Datensatz
            try {
                danach = arr.get(i + 1);//nächstes Datensatz
                aktDate = akt.getDatum();// aktuelles Datum
                nextDate = danach.getDatum();// nächstes Datum

                DateTime dt1 = new DateTime(aktDate);
                DateTime dt2 = new DateTime(nextDate);
                Duration duration = new Duration(dt1, dt2);
                long day_diff = duration.getStandardDays();

                long minutes_diff = duration.getStandardMinutes();

                if (minutes_diff == 15 && day_diff == 0 || minutes_diff == 15 && day_diff == 1) {
                    arrNew.add(akt);
                    double aktWert = akt.getVerbrauch();
                } else {
                    arrNew.add(akt);
                    newVerbrauch = akt.getVerbrauch();
                    double anzDatensaetze = (double) minutes_diff / (double) 15;
                    double wert_diff = danach.getVerbrauch() - akt.getVerbrauch();
                    double add_to_each15min = wert_diff / anzDatensaetze;

                    Date recursion = akt.getDatum();
                    while (!recursion.toString().equals(nextDate.toString())) {
                        newDate = addMinutesToDate(15, recursion);
                        newVerbrauch = newVerbrauch + add_to_each15min;
                        newTag.setWochentag(this.erkenneWochentag(newDate.getDay()));
                        Datensatz newDatensatz = new Datensatz();
                        newDatensatz.setDatum(newDate);
                        newDatensatz.setTag(newTag);
                        newDatensatz.setVerbrauch(newVerbrauch);
                        if (!newDate.toString().equals(nextDate.toString())) {
                            arrNew.add(newDatensatz);
                        }
                        recursion = newDate;
                    }
                }
            } catch (IndexOutOfBoundsException e) {
                arrNew.add(akt);//Letzten Datensatz hinzufügen
            }
        }
        newLastprofil.setDatensaetze(arrNew);
        //printLastprofil(newLastprofil);
        return newLastprofil;
    }

    /**
     * Ausgabe eines Lastprofils
     *
     * @param l
     */
    private void printLastprofil(Lastprofil l) {
        ArrayList<Datensatz> datenArr = l.getDatensaetze();
        Iterator<Datensatz> it = datenArr.iterator();
        while (it.hasNext()) {
            Datensatz d = it.next();
            System.out.println(d.getDatum() + " | " + d.getTag() + " | " + d.getVerbrauch());
        }
    }

    /**
     * Addiere Minuten zu aktuellem Datum.
     *
     * @param minutes
     * @param beforeTime
     * @return
     */
    private Date addMinutesToDate(int min, Date vorTime) {
        final long ONE_MINUTE_IN_MILLIS = 60000;//millisekunden
        long curTimeInMs = vorTime.getTime();
        Date afterAddingMins = new Date(curTimeInMs + (min * ONE_MINUTE_IN_MILLIS));
        return afterAddingMins;
    }

    /**
     * Liest von einer CSV-Datei die Zeilen ein, erstellt die jeweiligen
     * Entitaeten (Datensatz) und pflegt die Assoziation "datensaetze" (von
     * Entitaet Lastprofil zu Datensatz)
     *
     * @param kat Kategorie der CSV-Datei (Gas, Strom oder Fernwaerme)
     * @param dateiname Name der eingelesenen CSV-Datei, die die Verbrauchswerte
     * beinhaltet (inklusive Dateipfad)
     */
    public void erstelleEntityEinerKategorie(Kategorie kat, String dateiname) {
        Lastprofil betroffenesLastprofil = null;

        //Das Lastprofil der entsprechenden Kategorie wird ggf. instanziiert (falls nicht bereits geschehen)
        if (kat == Kategorie.Gas) {
            if (gas == null) {
                gas = new Lastprofil();
                gas.setTyp(Kategorie.Gas);
            }
            betroffenesLastprofil = gas;
        }
        if (kat == Kategorie.Strom) {
            if (strom == null) {
                strom = new Lastprofil();
                strom.setTyp(Kategorie.Strom);
            }
            betroffenesLastprofil = strom;
        }
        if (kat == Kategorie.Fernwaerme) {
            if (fernwaerme == null) {
                fernwaerme = new Lastprofil();
                fernwaerme.setTyp(Kategorie.Fernwaerme);
            }
            betroffenesLastprofil = fernwaerme;
        }

        Datensatz neueZeile = null;
        Date datum = null;
        double wert = 0;

        try {
            //erstellt RAF und verknüpft mit Datei; "r" erlaubt nur lesenden Zugriff
            RandomAccessFile file = new RandomAccessFile(dateiname, "r");

            String spalten[] = file.readLine().split(";");
            int anz = spalten.length;

            for (String line; (line = file.readLine()) != null;) {

                neueZeile = new Datensatz();

                //CSV besteht nur aus 2 Spalten (Datum und Uhrzeit in einer Spalte)
                if (anz == 2) {
                    String[] spalte = line.split(";");
                    String[] datumUndUhrzeit = spalte[0].split("\\s");
                    String tagMonatJahr[] = datumUndUhrzeit[0].split("\\.");

                    int tag = Integer.parseInt(tagMonatJahr[0]);
                    int monat = Integer.parseInt(tagMonatJahr[1]);
                    int jahr = Integer.parseInt(tagMonatJahr[2]);

                    jahr = jahr - 1900;
                    monat = monat - 1;

                    String uhrzeit[] = datumUndUhrzeit[1].split(":");

                    int stunde = Integer.parseInt(uhrzeit[0]);
                    int minute = Integer.parseInt(uhrzeit[1]);

                    datum = new Date(jahr, monat, tag, stunde, minute);
                    //ersetzt "," mit "." (wenn vorhanden)
                    String wertMitPunkt = spalte[1].replace(",", ".");
                    wert = Double.parseDouble(wertMitPunkt);

                    neueZeile.setDatum(datum);
                    neueZeile.setVerbrauch(wert);

                }

                //CSV besteht aus 3 Spalten (Datum und Uhrzeit getrennt)
                if (anz == 3) {
                    String[] spalte = line.split(";");
                    String tagMonatJahr[] = spalte[0].split("\\.");

                    int tag = Integer.parseInt(tagMonatJahr[0]);
                    int monat = Integer.parseInt(tagMonatJahr[1]);
                    int jahr = Integer.parseInt(tagMonatJahr[2]);

                    jahr = jahr - 1900;
                    monat = monat - 1;

                    String uhrzeit[] = spalte[1].split(":");

                    int stunde = Integer.parseInt(uhrzeit[0]);
                    int minute = Integer.parseInt(uhrzeit[1]);

                    datum = new Date(jahr, monat, tag, stunde, minute);
                    //ersetzt "," mit "." (wenn vorhanden)
                    String wertMitPunkt = spalte[2].replace(",", ".");
                    wert = Double.parseDouble(wertMitPunkt);

                    neueZeile.setDatum(datum);
                    neueZeile.setVerbrauch(wert);

                }

                //eingelesener Datensatz wird der Kategorie entsprechend dem Lastprofil hinzugefuegt
                if (kat == Kategorie.Gas) {
                    gas.addDatensatz(neueZeile);

                }
                if (kat == Kategorie.Strom) {
                    strom.addDatensatz(neueZeile);

                }
                if (kat == Kategorie.Fernwaerme) {
                    fernwaerme.addDatensatz(neueZeile);
                }

            }

            file.close();
        } catch (IOException ioex) {
            System.out.println("Folgende IOExpeption aufgetreten: " + ioex.getMessage());
        } catch (NumberFormatException nfex) {
            System.out.println("Folgende NumberFormatException aufgetreten: " + nfex.getMessage());
        } catch (ArrayIndexOutOfBoundsException arrayIndexEx) {
            System.out.println("Folgende ArrayIndexOutOfBoundsException aufgetreten: " + arrayIndexEx.getMessage());
        } catch (NullPointerException npex) {
            System.out.println("Folgende NullPointerException aufgetreten: " + npex.getMessage());
        } catch (java.lang.ClassCastException ccex) {
            System.out.println("Folgende ClassCastException aufgetreten: " + ccex.getMessage());
        }

    }

    /**
     * Erkennt das Format der CSV-Datei (wenn stetig steigend oder mit Abweichungen) 
     * und berechnet die Daten ggf. um 
     * (neue Daten werden wieder in den Entitaeten gespeichert)
     * 
     * @param lastp 0 = Gas, 1 = Strom, 2 = Fernwaerme
     */
    public void formatUmrechnen(int lastp) {
        Lastprofil betroffenesLastprofil = null;

        if (lastp == 0) {
            betroffenesLastprofil = gas;
        }
        if (lastp == 1) {
            betroffenesLastprofil = strom;
        }
        if (lastp == 2) {
            betroffenesLastprofil = fernwaerme;
        }

        boolean umrechnen = false;

        for (int i = 1; i < betroffenesLastprofil.getDatensaetze().size(); i++) {
            if (betroffenesLastprofil.getDatensaetze().get(i - 1).getVerbrauch() > betroffenesLastprofil.getDatensaetze().get(i).getVerbrauch()) {
                umrechnen = true;
                break;
            }
        }

        double summe = 0.0;
        
        if (umrechnen) {
            for (Datensatz datensatz : betroffenesLastprofil.getDatensaetze()) {
                summe += datensatz.getVerbrauch();
                datensatz.setVerbrauch(summe);
            }
        }

        //printEntities();

    }

    /**
     * Erkennt anhand des Parameters tag den Wochentag und gibt das
     * entsprechende Enum mit dem richtigen Wert zurueck
     *
     * @param tag Ein Integer-Wert zwischen 0 und 6 (0 steht fuer Sonntag, 1
     * fuer Montag usw.)
     * @return Enum Wochentag mit dem richtig dargestellten Wochentag
     */
    public Wochentag erkenneWochentag(int tag) {
        Wochentag rueckgabe;
        switch (tag) {
            case 0:
                rueckgabe = Wochentag.Sonntag;
                break;
            case 1:
                rueckgabe = Wochentag.Montag;
                break;
            case 2:
                rueckgabe = Wochentag.Dienstag;
                break;
            case 3:
                rueckgabe = Wochentag.Mittwoch;
                break;
            case 4:
                rueckgabe = Wochentag.Donnerstag;
                break;
            case 5:
                rueckgabe = Wochentag.Freitag;
                break;
            case 6:
                rueckgabe = Wochentag.Samstag;
                break;
            default:
                return null;
        }
        return rueckgabe;
    }

    /**
     * Fuegt zu einem Wochentag eines Lastprofils die Exemplare hinzu. Beispiel:
     * Zu allen Montagen wird ein weiterer Montag hinzugefuegt (Assoziation von
     * "AlleTage" zu "Tag")
     *
     * @param lastprofil Lastprofil-Objekt
     * @param neuerTag Neuer Tag (Objekt Tag), der hinzugefuegt wird
     * @param tag Integer zwischen 0 und 6, welches den Wochentag repraesentiert
     * (0 Sonntag, 1 Montag usw.)
     */
    public void fuegeTagZuAllenTagenHinzu(Lastprofil lastprofil, Tag neuerTag, int tag) {
        if (tag < 0 || tag > 6) {
            return;
        }

        //Falls nicht bereits geschehen, werden zuerst die 7 Durchschnittstage des Lastprofils instanziiert
        if (lastprofil.getWochentage().size() != 7) {
            this.instanziiereDurchschnittstageFuerLastprofil(lastprofil);
        }

        AlleTage alleTage = lastprofil.getWochentagByIndex(tag);
        alleTage.addTag(neuerTag);

    }

    /**
     * Instanziiert fuer ein Lastprofil die Liste der Durchschnittstage mit 7
     * neuen Objekten (um zukuenftige Exceptions zu vermeiden)
     *
     * @param lastprofil Lastprofil, dessen Durschnitts-Wochentage gefuellt
     * werden sollen
     */
    public void instanziiereDurchschnittstageFuerLastprofil(Lastprofil lastprofil) {
        for (int i = 0; i < 7; i++) {
            lastprofil.addWochentag(new AlleTage());
            lastprofil.getWochentagByIndex(i).setWochentag(this.erkenneWochentag(i));
        }
    }

    /**
     * Erstellt aus den eingelesenen Datensaetzen die Entitaeten, die jeweils
     * einem Tag entsprechen.
     *
     * @param lastp Integer-Wert zwischen 0 und 2, der einer
     * Lastprofil-Kategorie entspricht (0 = Gas, 1 = Strom, 2 = Fernwaerme).
     */
    public void erstelleWochentageEinerKategorie(int lastp) {
        Lastprofil lastprofil = null;
        ArrayList<Datensatz> datensaetze = null;

        switch (lastp) {
            case 0:
                datensaetze = gas.getDatensaetze();
                lastprofil = gas;
                break;
            case 1:
                datensaetze = strom.getDatensaetze();
                lastprofil = strom;
                break;
            case 2:
                datensaetze = fernwaerme.getDatensaetze();
                lastprofil = fernwaerme;
                break;
            default:
                break;
        }

        int anzDs = datensaetze.size();

        Datensatz aktuellerDatensatz;
        Date datumAktDatensatz;
        Datensatz naechsterDatensatz;
        Date datumNextDatensatz;

        Tag neuerTag = null;

        for (int i = 0; i < anzDs; i++) {
            aktuellerDatensatz = datensaetze.get(i);
            datumAktDatensatz = (Date) aktuellerDatensatz.getDatum().clone();
            datumAktDatensatz.setHours(0);
            datumAktDatensatz.setMinutes(0);

            //letzter Durchlauf der Datensaetze -> kein naechster Datensatz mehr
            if (i == anzDs - 1) {
                naechsterDatensatz = null;
                datumNextDatensatz = null;
                //evtl Werte von aktDs uebernehmen?!
            } else {
                naechsterDatensatz = datensaetze.get(i + 1);
                datumNextDatensatz = (Date) naechsterDatensatz.getDatum().clone();
                datumNextDatensatz.setHours(0);
                datumNextDatensatz.setMinutes(0);
            }

            if (i == 0) {
                neuerTag = new Tag();
                neuerTag.setTyp(lastprofil.getTyp());
                neuerTag.setDatum(aktuellerDatensatz.getDatum());

                neuerTag.setWochentag(this.erkenneWochentag(datumAktDatensatz.getDay()));
            }

            //Datensaetze werden dem Tag hinzugefuegt
            neuerTag.addDatensatz(aktuellerDatensatz);

            if (datumNextDatensatz != null) {

                //bei jedem "Tagesumbruch" (in der Regel nach 96 Datensaetzen
                if (datumAktDatensatz.before(datumNextDatensatz)) {

                    //mit Ds gefuellter Tag wird dem Lastprofil hinzugefuegt
                    lastprofil.addTag(neuerTag);

                    this.fuegeTagZuAllenTagenHinzu(lastprofil, neuerTag, datumAktDatensatz.getDay());
                    //fuer den naechsten Tag wird das Tag-Objekt wieder neu instanziiert
                    neuerTag = new Tag();
                    neuerTag.setTyp(lastprofil.getTyp());
                    neuerTag.setDatum(datumNextDatensatz);

                    neuerTag.setWochentag(this.erkenneWochentag(datumNextDatensatz.getDay()));

                }
            }

            //allerletzter Tag
            if (datumNextDatensatz == null) {
                //mit Ds gefuellter Tag wird dem Lastprofil hinzugefuegt
                lastprofil.addTag(neuerTag);
                this.fuegeTagZuAllenTagenHinzu(lastprofil, neuerTag, datumAktDatensatz.getDay());
            }

        }

        //Berechnung der Kennwerte
        this.berechneKennwerteFuerEinzelneTage(lastprofil);
        this.berechneKennwerteFuerDurchschnittstage(lastprofil);

        //Kontrollausgabe
        //this.printEntities();
    }

    /**
     * Berechnet die 3 Werte Grundlast, Mittellast und Spitzenlast fuer jeden
     * einzelnen Tag und speichert diese in den enstprechenden Entitaeten "Tag"
     *
     * @param lastprofil Lastprofil
     */
    public void berechneKennwerteFuerEinzelneTage(Lastprofil lastprofil) {
        ArrayList<Datensatz> zuBerechnendeDs = new ArrayList<Datensatz>();
        for (Tag tag : lastprofil.getBesteht()) {
            for (Datensatz ds : tag.getDatensaetze()) {
                zuBerechnendeDs.add(ds);
            }

            tag.setGrundlast(this.grundlast(zuBerechnendeDs));
            tag.setMittellast(this.mittellast(zuBerechnendeDs));
            tag.setSpitzenlast(this.spitzenlast(zuBerechnendeDs));
            zuBerechnendeDs.clear();
        }
    }

    /**
     * Berechnet die 3 Werte Grundlast, Mittellast und Spitzenlast fuer die 7
     * Durchschnittstage eines Lastprofils. Es werden alle Montage geholt und
     * daraus werden die Werte berechnet. Diese werden im "AlleTage"-Objekt fuer
     * den Montag gespeichert. Analog fuer Dienstag - Sonntag.
     *
     * @param lastprofil Lastprofil-Objekt
     */
    public void berechneKennwerteFuerDurchschnittstage(Lastprofil lastprofil) {
        double grundlastDurchschnitt = 0.0;
        double mittellastDurchschnitt = 0.0;
        double spitzenlastDurchschnitt = 0.0;
        int anz = 0;
        for (AlleTage alleTage : lastprofil.getWochentage()) {
            anz = alleTage.getTage().size();
            for (Tag tag : alleTage.getTage()) {
                grundlastDurchschnitt += tag.getGrundlast();
                mittellastDurchschnitt += tag.getMittellast();
                spitzenlastDurchschnitt += tag.getSpitzenlast();
            }
            grundlastDurchschnitt /= anz;
            mittellastDurchschnitt /= anz;
            spitzenlastDurchschnitt /= anz;
            alleTage.setGrundlast(grundlastDurchschnitt);
            alleTage.setMittellast(mittellastDurchschnitt);
            alleTage.setSpitzenlast(spitzenlastDurchschnitt);
        }

        /*
        ArrayList<Datensatz> zuBerechnendeDs = new ArrayList<Datensatz>();
        for (AlleTage alleTage : lastprofil.getWochentage()) {
            for (Tag tag : alleTage.getTage()) {
                for (Datensatz ds : tag.getDatensaetze()) {
                    zuBerechnendeDs.add(ds);
                }
            }
            alleTage.setGrundlast(this.grundlast(zuBerechnendeDs));
            alleTage.setMittellast(this.mittellast(zuBerechnendeDs));
            alleTage.setSpitzenlast(this.spitzenlast(zuBerechnendeDs));

            zuBerechnendeDs.clear();
        }
         */
    }

    /**
     * Methode, die zur Kontrolle stichpunktartig die Werte der aktuell
     * eingelesenen und gespeicherten Lastprofile in der Konsole ausgibt.
     */
    public void printEntities() {
        try {
            System.out.println("\nAusgabe aller aktuellen Entitaeten:\n");
            if (gas != null) {
                System.out.println("Gas-Lastprofil ist bereits instanziiert: ");
                System.out.println("Anzahl Datensaetze: " + gas.getDatensaetze().size());
                System.out.println("\nErster Datensatz: ");
                System.out.println("Datum: " + gas.getDatensaetze().get(0).getDatum());
                System.out.println("Verbrauch: " + gas.getDatensaetze().get(0).getVerbrauch());
                System.out.println("\nZweiter Datensatz: ");
                System.out.println("Datum: " + gas.getDatensaetze().get(1).getDatum());
                System.out.println("Verbrauch: " + gas.getDatensaetze().get(1).getVerbrauch());
                System.out.println("\nLetzter Datensatz: ");
                System.out.println("Datum: " + gas.getDatensaetze().get(gas.getDatensaetze().size() - 1).getDatum());
                System.out.println("Verbrauch: " + gas.getDatensaetze().get(gas.getDatensaetze().size() - 1).getVerbrauch());
                System.out.println("\nAnzahl Tage: " + gas.getBesteht().size());
                System.out.println("Anzahl Datensaetze erster Tag: " + gas.getBesteht().get(0).getDatensaetze().size());
                System.out.println("Anzahl Datensaetze letzter Tag: " + gas.getBesteht().get(gas.getBesteht().size() - 1).getDatensaetze().size());
                System.out.println("Letzter Tag: " + gas.getBesteht().get(gas.getBesteht().size() - 1).getWochentag());
                System.out.println("Allerletzter Datensatz: " + gas.getBesteht().get(gas.getBesteht().size() - 1).getDatensaetze().get(gas.getBesteht().get(gas.getBesteht().size() - 1).getDatensaetze().size() - 1).getDatum()
                        + " " + gas.getBesteht().get(gas.getBesteht().size() - 1).getDatensaetze().get(gas.getBesteht().get(gas.getBesteht().size() - 1).getDatensaetze().size() - 1).getVerbrauch());
                if (gas.getWochentage().size() == 7) {
                    System.out.println("Anzahl Montage: " + gas.getWochentagByIndex(1).getTage().size());
                    System.out.println("Erster Montag erster Datensatz: ");
                    System.out.println("Datum: " + gas.getWochentagByIndex(1).getTage().get(0).getDatensaetze().get(0).getDatum());
                    System.out.println("Verbrauch: " + gas.getWochentagByIndex(1).getTage().get(0).getDatensaetze().get(0).getVerbrauch());
                    System.out.println("Anzahl Freitage: " + gas.getWochentagByIndex(5).getTage().size());
                    System.out.println("Erster Freitag erster Datensatz: ");
                    System.out.println("Datum: " + gas.getWochentagByIndex(5).getTage().get(0).getDatensaetze().get(0).getDatum());
                    System.out.println("Verbrauch: " + gas.getWochentagByIndex(5).getTage().get(0).getDatensaetze().get(0).getVerbrauch());

                }
            } else {
                System.out.println("Gas-Lastprofil ist gleich null.\n");
            }
            if (strom != null) {
                System.out.println("\nStrom-Lastprofil ist bereits instanziiert: ");
                System.out.println("Anzahl Datensaetze: " + strom.getDatensaetze().size());
                System.out.println("\nErster Datensatz: ");
                System.out.println("Datum: " + strom.getDatensaetze().get(0).getDatum());
                System.out.println("Verbrauch: " + strom.getDatensaetze().get(0).getVerbrauch());
                System.out.println("\nLetzter Datensatz: ");
                System.out.println("Datum: " + strom.getDatensaetze().get(strom.getDatensaetze().size() - 1).getDatum());
                System.out.println("Verbrauch: " + strom.getDatensaetze().get(strom.getDatensaetze().size() - 1).getVerbrauch());
                System.out.println("\nAnzahl Tage: " + strom.getBesteht().size());
                System.out.println("Anzahl Datensaetze erster Tag: " + strom.getBesteht().get(0).getDatensaetze().size());
                System.out.println("Anzahl Datensaetze letzter Tag: " + strom.getBesteht().get(strom.getBesteht().size() - 1).getDatensaetze().size());
                if (strom.getWochentage().size() == 7) {
                    System.out.println("Anzahl Montage: " + strom.getWochentagByIndex(1).getTage().size());
                    System.out.println("Erster Montag erster Datensatz: ");
                    System.out.println("Datum: " + strom.getWochentagByIndex(1).getTage().get(0).getDatensaetze().get(0).getDatum());
                    System.out.println("Verbrauch: " + strom.getWochentagByIndex(1).getTage().get(0).getDatensaetze().get(0).getVerbrauch());
                    System.out.println("Anzahl Freitage: " + strom.getWochentagByIndex(5).getTage().size());
                    System.out.println("Erster Freitag erster Datensatz: ");
                    System.out.println("Datum: " + strom.getWochentagByIndex(5).getTage().get(0).getDatensaetze().get(0).getDatum());
                    System.out.println("Verbrauch: " + strom.getWochentagByIndex(5).getTage().get(0).getDatensaetze().get(0).getVerbrauch());

                }
            } else {
                System.out.println("Strom-Lastprofil ist gleich null.\n");
            }
            if (fernwaerme != null) {
                System.out.println("\nFernwaerme-Lastprofil ist bereits instanziiert: ");
                System.out.println("Anzahl Datensaetze: " + fernwaerme.getDatensaetze().size());
                System.out.println("\nErster Datensatz: ");
                System.out.println("Datum: " + fernwaerme.getDatensaetze().get(0).getDatum());
                System.out.println("Verbrauch: " + fernwaerme.getDatensaetze().get(0).getVerbrauch());
                System.out.println("\nLetzter Datensatz: ");
                System.out.println("Datum: " + fernwaerme.getDatensaetze().get(fernwaerme.getDatensaetze().size() - 1).getDatum());
                System.out.println("Verbrauch: " + fernwaerme.getDatensaetze().get(fernwaerme.getDatensaetze().size() - 1).getVerbrauch());
                System.out.println("\nAnzahl Tage: " + fernwaerme.getBesteht().size());
                System.out.println("Anzahl Datensaetze erster Tag: " + fernwaerme.getBesteht().get(0).getDatensaetze().size());
                System.out.println("Anzahl Datensaetze letzter Tag: " + fernwaerme.getBesteht().get(fernwaerme.getBesteht().size() - 1).getDatensaetze().size());
                if (fernwaerme.getWochentage().size() == 7) {
                    System.out.println("Anzahl Montage: " + fernwaerme.getWochentagByIndex(1).getTage().size());
                    System.out.println("Erster Montag erster Datensatz: ");
                    System.out.println("Datum: " + fernwaerme.getWochentagByIndex(1).getTage().get(0).getDatensaetze().get(0).getDatum());
                    System.out.println("Verbrauch: " + fernwaerme.getWochentagByIndex(1).getTage().get(0).getDatensaetze().get(0).getVerbrauch());
                    System.out.println("Anzahl Freitage: " + fernwaerme.getWochentagByIndex(5).getTage().size());
                    System.out.println("Erster Freitag erster Datensatz: ");
                    System.out.println("Datum: " + fernwaerme.getWochentagByIndex(5).getTage().get(0).getDatensaetze().get(0).getDatum());
                    System.out.println("Verbrauch: " + fernwaerme.getWochentagByIndex(5).getTage().get(0).getDatensaetze().get(0).getVerbrauch());
                }
            } else {
                System.out.println("Fernwaerme-Lastprofil ist gleich null.\n");
            }
        } catch (IndexOutOfBoundsException ex) {
            System.out.println("IndexOutOfBoundsException: " + ex.getMessage());
        }

    }

    /**
     * Hilfsmethode, die die Methode grundlastBerechnen() aufruft
     *
     * @param datensaetze Datensaetze, zu denen die Grundlast berechnet werden
     * soll
     * @return Wert der Grundlast
     */
    public double grundlast(ArrayList<Datensatz> datensaetze) {

        Lastprofil l = new Lastprofil();
        for (Datensatz d : datensaetze) {
            l.addDatensatz(d);
        }

        return this.grundlastBerechnen(l);
    }

    /**
     * Hilfsmethode, die die Methode mitellastBerechnen() aufruft
     *
     * @param datensaetze Datensaetze, zu denen die Mittellast berechnet werden
     * soll
     * @return Wert der Mittellast
     */
    public double mittellast(ArrayList<Datensatz> datensaetze) {

        Lastprofil l = new Lastprofil();
        for (Datensatz d : datensaetze) {
            l.addDatensatz(d);
        }

        return this.mittellastBerechnen(l);
    }

    /**
     * Hilfsmethode, die die Methode spitzenlastBerechnen() aufruft
     *
     * @param datensaetze Datensaetze, zu denen die Spitzenlast berechnet werden
     * soll
     * @return Wert der Spitzenlast
     */
    public double spitzenlast(ArrayList<Datensatz> datensaetze) {

        Lastprofil l = new Lastprofil();
        for (Datensatz d : datensaetze) {

            l.addDatensatz(d);
        }

        return this.spitzenlastBerechnen(l);

    }

}
