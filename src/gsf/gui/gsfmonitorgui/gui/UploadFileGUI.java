/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gsf.gui.gsfmonitorgui.gui;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import static javax.swing.JOptionPane.showMessageDialog;

/**
 *
 * @author gromow
 */
public class UploadFileGUI extends javax.swing.JFrame {

    //Gerade ausgewähle Kategorie zum Hochladen
    private static String currentFileName;
    private String pfad;
    /**
     * Creates new form UploadFileGUI
     */
    public UploadFileGUI() {
        initComponents();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        open = new javax.swing.JFileChooser();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        open.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                openActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(open, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(open, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    //Gerade ausgewähle Kategorie zum Hochladen
    public static void setCurrentFileName(String currentFileName) {
        UploadFileGUI.currentFileName = currentFileName;
    }


    private void openActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_openActionPerformed

        if (evt.getActionCommand().equals("ApproveSelection")) {
            System.out.println("opened");
            File file = open.getSelectedFile();
            pfad = getPfadByOS();
            File destinationFile = new File(pfad, file.getName());
            try {

                Files.copy(file.toPath(), destinationFile.toPath());
                //System.out.println("Button: "+ currentFileName);
                // Set Labels
                if (currentFileName.equals("strom")) {
                    StartGUI.setStromLabelText(file.getName());
                } else if (currentFileName.equals("gas")) {
                    StartGUI.setGasL(file.getName());
                } else {
                    StartGUI.setFwLabelText(file.getName());
                }

            } catch (IOException ex) {
                showMessageDialog(null, "Datei " + file.getName() + " ist bereits vorhanden!");
                System.out.println( "Datei " + file.getName() + " ist bereits vorhanden!");
                //ex.printStackTrace();
            }
            setDefaultCloseOperation(DISPOSE_ON_CLOSE);
            dispose();
        } else if (evt.getActionCommand().equals("CancelSelection")) {
            System.out.println("closed");
            setDefaultCloseOperation(DISPOSE_ON_CLOSE);
            dispose();
        }


    }//GEN-LAST:event_openActionPerformed

    
    private String getPfadByOS(){
        /*
        String ret = System.getProperty("os.name").toLowerCase();
        System.out.println("GSFMonitorChartImpl:"+ret);
        if(ret.equals("mac os x")){
                this.pfad = "/users/upload";
        }else if(ret.equals("windows 10")){
                this.pfad = "c:\\upload";
        }else if(ret.equals("windows 8.1")){
                this.pfad = "c:\\upload";
        }else{
            this.pfad = "";
        }
        
        return this.pfad;
        */
        final String folder = System.getProperty("user.dir");
        File dir = new File(folder+"/upload");
        return folder+"/upload";
        
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(UploadFileGUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(UploadFileGUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(UploadFileGUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(UploadFileGUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new UploadFileGUI().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JFileChooser open;
    // End of variables declaration//GEN-END:variables

}
