/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gsf.jfreechart.impl;

import gsf.jfreechart.service.IJFreeChart;
import java.io.File;
import java.io.IOException;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.DefaultCategoryDataset;

/**
 *
 * @author gromow ,Patrick Sojka
 */
public class GSFMonitorChartImpl implements IJFreeChart {

    private String pfad;
    private String fileName;
    private JFreeChart JFreeChartObject;
    private String currentPfad;

    /**
     * Diese Methode erstellt ein Liniendiagramm und gibt ein String wieder mit
     * dem Pfad zur .png datei die an die Steuerungsklasse geschickt wird.
     *
     * @param dataset
     * @param typ
     * @return
     */
    @Override
    public String TimeSeries(DefaultCategoryDataset dataset, String typ) {

        Random rand = new Random();
        int randomNumGas = rand.nextInt((1000000 - 5000) + 1) + 200;
        int randomNumStrom = rand.nextInt((20 + 3000) + 2) - 100;
        int randomNumFernwaerme = rand.nextInt((2000000 / 20) + 30) + 500;

        try {
            if (typ.equals("Gas")) {

                JFreeChartObject = ChartFactory.createLineChart(
                        "Gas", "",
                        "Kubikmeter",
                        dataset, PlotOrientation.VERTICAL,
                        true, true, false);
                fileName = "/TimeseriesGas" + randomNumGas + ".png";

            } else if (typ.equals("Strom")) {

                JFreeChartObject = ChartFactory.createLineChart(
                        "Strom", "",
                        "kW/h",
                        dataset, PlotOrientation.VERTICAL,
                        true, true, false);
                fileName = "/TimeseriesStrom" + randomNumStrom + ".png";

            } else if (typ.equals("Fernwaerme")) {

                JFreeChartObject = ChartFactory.createLineChart(
                        "Fernwaerme", "",
                        "kW/h",
                        dataset, PlotOrientation.VERTICAL,
                        true, true, false);
                fileName = "/TimeseriesFernwaerme" + randomNumFernwaerme + ".png";

            }
        } catch (Exception e) {
            System.out.println("Bild " + e.getMessage());
        }
        try {

            currentPfad = this.getPfadByOS();
            exportChartToPng(currentPfad + fileName, JFreeChartObject, 780, 230);

        } catch (IOException ex) {
            Logger.getLogger(GSFMonitorChartImpl.class.getName()).log(Level.SEVERE, null, ex);
        }

        return currentPfad + fileName;
    }

    /*
    @Override
    public String Demo(String diagramm) {
        
        DefaultCategoryDataset dataset;
       
        
        if(diagramm.equals("diagramm1")){
            
            dataset = createDataset1();
            JFreeChartObject = ChartFactory.createLineChart(
                "Gas","",
                "Milliarden Kubikmeter",
                dataset,PlotOrientation.VERTICAL,
                true,true,false);
            fileName = "/filename.png";
            
        }else if (diagramm.equals("diagramm2")) {
            
            dataset = createDataset2();
            JFreeChartObject = ChartFactory.createLineChart(
                "Strom","",
                "kW/h",
                dataset,PlotOrientation.VERTICAL,
                true,true,false);
            fileName = "/filename2.png";
            
        }else{
           dataset = createDataset3();
            JFreeChartObject = ChartFactory.createBarChart("Strom", "", "kW/h", dataset);
            fileName = "/filename3.png";
        }

        try {
            
            currentPfad = this.getPfadByOS();
            exportChartToPng(currentPfad+fileName, JFreeChartObject, 655, 200);
            
        } catch (IOException ex) {
            Logger.getLogger(GSFMonitorChartImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return currentPfad+fileName;
    }
     */
    private void exportChartToPng(String name, JFreeChart chart, int x, int y) throws IOException {
        File outputFile = new File(name);
        chart.setBackgroundPaint(null);
        ChartUtilities.saveChartAsPNG(outputFile, chart, x, y);
    }

    /**
     * liefert OS-Pfad zurück. Abhängig von OS
     *
     * @return
     */
    private String getPfadByOS() {

        final String folder = System.getProperty("user.dir");
        File dir = new File(folder + "/upload");
        return folder + "/upload";
    }

    /**
     * Diese Methode erstellt ein Säulendiagramm und gibt ein String wieder mit
     * dem Pfad zur .png datei die an die Steuerungsklasse geschickt wird.
     *
     * @param dataset
     * @param typ
     * @return
     */
    @Override
    public String BarChart(DefaultCategoryDataset dataset, String typ) {

        Random rand = new Random();
        int randomNumGas1 = rand.nextInt((100000 - 5000) + 1) + 200;
        int randomNumStrom1 = rand.nextInt((20 + 30000) + 2) - 100;
        int randomNumFernwaerme1 = rand.nextInt((200000 / 20) + 30) + 500;

        if (typ.equals("Gas")) {

            JFreeChartObject = ChartFactory.createBarChart(
                    "Gas", "",
                    "Kubikmeter",
                    dataset, PlotOrientation.VERTICAL,
                    true, true, false);
            fileName = "/BarChartGas" + randomNumGas1 + ".png";

        } else if (typ.equals("Strom")) {

            JFreeChartObject = ChartFactory.createBarChart(
                    "Strom", "",
                    "kW/h",
                    dataset, PlotOrientation.VERTICAL,
                    true, true, false);
            fileName = "/BarChartStrom" + randomNumStrom1 + ".png";

        } else if (typ.equals("Fernwaerme")) {

            JFreeChartObject = ChartFactory.createBarChart(
                    "Strom", "",
                    "kW/h",
                    dataset, PlotOrientation.VERTICAL,
                    true, true, false);
            fileName = "/BarChartFernwaerme" + randomNumFernwaerme1 + ".png";

        }
        try {

            currentPfad = this.getPfadByOS();
            exportChartToPng(currentPfad + fileName, JFreeChartObject, 780, 230);

        } catch (IOException ex) {
            Logger.getLogger(GSFMonitorChartImpl.class.getName()).log(Level.SEVERE, null, ex);
        }

        return currentPfad + fileName;
    }

    /**
     * HD Auflösung von TimeSeries
     *
     * @param dataset
     * @param typ
     * @return
     */
    public String TimeSeriesHD(DefaultCategoryDataset dataset, String typ) {

        Random rand = new Random();
        int randomNum = rand.nextInt((1000000 - 5000) + 1) + 200;
        //System.out.println("rand:"+randomNum);

        if (typ.equals("Gas")) {

            JFreeChartObject = ChartFactory.createLineChart(
                    "Gas", "",
                    "Kubikmeter",
                    dataset, PlotOrientation.VERTICAL,
                    true, true, false);
            fileName = "/HD_TG_" + randomNum + ".png";

        } else if (typ.equals("Strom")) {

            JFreeChartObject = ChartFactory.createLineChart(
                    "Strom", "",
                    "kW/h",
                    dataset, PlotOrientation.VERTICAL,
                    true, true, false);
            fileName = "/HD_TS_" + randomNum + ".png";

        } else if (typ.equals("Fernwaerme")) {

            JFreeChartObject = ChartFactory.createLineChart(
                    "Strom", "",
                    "kW/h",
                    dataset, PlotOrientation.VERTICAL,
                    true, true, false);
            fileName = "/HD_TF_" + randomNum + ".png";

        }
        try {

            currentPfad = this.getPfadByOS();
            exportChartToPng(currentPfad + fileName, JFreeChartObject, 1920, 1080);

        } catch (IOException ex) {
            Logger.getLogger(GSFMonitorChartImpl.class.getName()).log(Level.SEVERE, null, ex);
        }

        return currentPfad + fileName;
    }

    /**
     * HD Auflösung von BarChart
     *
     * @param dataset
     * @param typ
     * @return
     */
    public String BarChartHD(DefaultCategoryDataset dataset, String typ) {

        Random rand = new Random();
        int randomNum = rand.nextInt((1000000 - 5000) + 1) + 200;

        if (typ.equals("Gas")) {

            JFreeChartObject = ChartFactory.createBarChart(
                    "Gas", "",
                    "Kubikmeter",
                    dataset, PlotOrientation.VERTICAL,
                    true, true, false);
            fileName = "/HD_BG_" + randomNum + ".png";

        } else if (typ.equals("Strom")) {

            JFreeChartObject = ChartFactory.createBarChart(
                    "Strom", "",
                    "kW/h",
                    dataset, PlotOrientation.VERTICAL,
                    true, true, false);
            fileName = "/HD_SG_" + randomNum + ".png";

        } else if (typ.equals("Fernwaerme")) {

            JFreeChartObject = ChartFactory.createBarChart(
                    "Strom", "",
                    "kW/h",
                    dataset, PlotOrientation.VERTICAL,
                    true, true, false);
            fileName = "/HD_FG_" + randomNum + ".png";

        }
        try {

            currentPfad = this.getPfadByOS();
            exportChartToPng(currentPfad + fileName, JFreeChartObject, 1920, 1080);

        } catch (IOException ex) {
            Logger.getLogger(GSFMonitorChartImpl.class.getName()).log(Level.SEVERE, null, ex);
        }

        return currentPfad + fileName;
    }
}
