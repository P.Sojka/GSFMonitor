/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gsf.jfreechart.service;

import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.data.time.TimeSeries;

/**
 *
 * @author gromow
 */

public interface IJFreeChart {
   // public String Demo(String diagramm);
    public String TimeSeries(DefaultCategoryDataset dataset,String typ);
    public String BarChart(DefaultCategoryDataset dataset,String typ);
//    public String SampleChart(DefaultCategoryDataset dataset);
//    public String StackBarChart(DefaultCategoryDataset dataset);

}
